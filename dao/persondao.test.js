var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "secret",
    database: "supertestdb",
    debug: false,
    multipleStatements: true
});


let personDao = new PersonDao(pool);

beforeAll(done => {
    runsqlfile("dao/create_tables.sql", pool, () => {
        runsqlfile("dao/create_testdata.sql", pool, done);
    });
});

afterAll(() => {
    pool.end();
});

test("get one person from db", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBe(1);
        expect(data[0].navn).toBe("Hei Sveisen");
        done();
    }

    personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.length).toBe(0);
        done();
    }

    personDao.getOne(0, callback);
});

test("add person to db", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data=" + JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    personDao.createOne({
            navn: "Nils Nilsen",
            alder: 34,
            adresse: "Gata 3"
        },
        callback
    );
});

test("get all persons from db", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ", data.length=" + data.length
        );
        expect(data.length).toBeGreaterThanOrEqual(2);
        done();
    }

    personDao.getAll(callback);
});

//______________________________
test("update person in db", done => {
    function callback(status, data) {
        console.log(
            "Test callback: status=" + status + ",data.length=" + data.length
        );
        expect(data[0].navn).toBe("Torje Thorkildsen");
        done();
    }

    let id = 1;
    personDao.updateOne(
        id, {
            navn: "Torje Thorkildsen",
            alder: 93,
            adresse: "abc1"
        },
        function(status, data) {
            personDao.getOne(id, callback);
        }
    );
});

test("delete person from db", done => {
    var ant = 0;

    function callback(status, data) {
        console.log("Test callback: status= " + status + ", data.length =" + data.length);
        expect(data.length).toBe(ant);
        done();
    }

    personDao.getAll(function(status, data) {
        ant = data.length;

        console.log("blaaaaa", data[0], data[0].id);
        personDao.deleteOne(data[0].id, function(status, data) {
            personDao.getAll(callback);
        });
    })
});